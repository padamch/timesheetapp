<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$app->get('/users', 'getUsers');
$app->get('/users/:id', 'getUser');
$app->get('/users/check_username/:username', 'checkUsername');
$app->get('/users/check_email/:email', 'checkEmail');
$app->get('/users/check_code/:email/:code','checkCode'); 

$app->post('/users/register', 'addUsers');
$app->post('/users/login', 'login'); 
$app->post('/users/forget_password','forgetPassword');
$app->put('/users/reset_password','resetPassword');

$app->get('/companies/:user_id', 'getCompanies');
$app->get('/company/check_company/:user_id/:company_name', 'checkCompany');
$app->get('/company/:id', 'getCompany');
$app->post('/company/add', 'addCompany');
$app->put('/company/edit', 'editCompany');
$app->delete('/company/delete/:id', 'deleteCompany');

$app->get('/monthsetting/:user_id/:company_id', 'getMonthSetting');
$app->post('/monthsetting/add', 'addMonthSetting');
$app->put('/monthsetting/edit', 'editMonthSetting');
$app->delete('/monthsetting/delete/:id', 'deleteMonthSetting');

$app->get('/timesheet/pagination/:user_id/:company_id/:month/:start_item/:items_per_page','getTimesheet');
$app->get('/timesheet/totalitems/:user_id/:month', 'getTotalTimesheet');
$app->post('/timesheet/add', 'addTimesheet');
$app->put('/timesheet/edit', 'editTimesheet');
$app->delete('/timesheet/delete', 'deleteTimesheet');

$app->get('/users/userdata/:username', 'userData');

$app->run();

function userData($username) {
  require './modal/users.php';
  $users = new Users;
  header('Content-Type: application/json');
  echo $users->userData($username);
  exit;
}

function getUsers() {
  require './modal/users.php';
  $users = new Users;
  header('Content-Type: application/json');
  echo $users->getUsers();
  exit;
}

function getUser($id) {
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->getUser($id);
  exit;
}

function checkUsername($username) {
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->checkUsername($username);
  exit;
}

function checkEmail($email) {
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->checkEmail($email);
  exit;
}

function login(){
  $request = \Slim\Slim::getInstance()->request();
  $user_info = json_decode($request->getBody());
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->login($user_info);
  exit;
}

function forgetPassword(){
  $request = \Slim\Slim::getInstance()->request();
  $user_info = json_decode($request->getBody());
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->forgetPassword($user_info);
  exit;
}

function resetPassword(){
  $request = \Slim\Slim::getInstance()->request();
  $user_info = json_decode($request->getBody());
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->resetPassword($user_info);
  exit;
}

function checkCode($email,$code){
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo json_encode($user->checkCode($email,$code));
  exit;
}

function addUsers() {
  /* To retrieve data from traditional POST
  $app1 = \Slim\Slim::getInstance()->request();
  echo $app1->params('username');
  */
  $request = \Slim\Slim::getInstance()->request();
  $user_info = json_decode($request->getBody());
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->addUsers($user_info);
  exit;

}

function getCompanies($user_id){
  require './modal/companies.php';
  $companies=new Companies;
  header('Content-Type: application/json');
  echo $companies->getCompanies($user_id);
  exit;
}

function getCompany($id){
  require './modal/companies.php';
  $company=new Companies;
  header('Content-Type: application/json');
  echo $company->getCompany($id);
  exit;
}

function checkCompany($user_id,$company_name){
  require './modal/companies.php';
  $company=new Companies;
  header('Content-Type: application/json');
  echo $company->checkCompany($user_id,$company_name);
  exit;
}

function addCompany(){
  $request = \Slim\Slim::getInstance()->request();
  $company_info = json_decode($request->getBody());
  require './modal/companies.php';
  $company = new Companies;
  header('Content-Type: application/json');
  echo $company->addCompany($company_info);
  exit;
}

function editCompany(){
  $request = \Slim\Slim::getInstance()->request();
  $edit_info = json_decode($request->getBody());
  require './modal/companies.php';
  $company = new Companies;
  header('Content-Type: application/json');
  echo $company->editCompany($edit_info);
  exit;
}

function deleteCompany($id){
  require './modal/companies.php';
  $company=new Companies;
  header('Content-Type: application/json');
  echo $company->deleteCompany($id);
  exit;
}

function getMonthSetting($user_id,$company_id){
  require './modal/monthsettings.php';
  $setting=new Monthsettings;
  header('Content-Type: application/json');
  echo $setting->getMonthSetting($user_id,$company_id);
  exit;
}

function addMonthSetting(){
  $request = \Slim\Slim::getInstance()->request();
  $setting_info = json_decode($request->getBody());
  require './modal/monthsettings.php';
  $setting = new Monthsettings;
  header('Content-Type: application/json');
  echo $setting->addMonthSetting($setting_info);
  exit;
}

function editMonthSetting(){
  $request = \Slim\Slim::getInstance()->request();
  $edit_info = json_decode($request->getBody());
  require './modal/monthsettings.php';
  $setting = new Monthsettings;
  header('Content-Type: application/json');
  echo $setting->editMonthSetting($edit_info);
  exit;
}

function deleteMonthSetting($id){
  require './modal/monthsettings.php';
  $setting=new Monthsettings;
  header('Content-Type: application/json');
  echo $setting->deleteMonthSetting($id);
  exit;
}

function getTimesheet($user_id,$company_id,$month,$start_item,$items_per_page){
  require './modal/timesheet.php';
  $timesheet= new Timesheet;
  header('Content-Type: application/json');
  echo $timesheet->getTimesheet($user_id,$company_id,$month,$start_item,$items_per_page);
  exit;
}


function getTotalTimesheet($user_id, $month)
{
  require './modal/timesheet.php';
  $timesheet= new Timesheet;
  header('Content-Type: application/json');
  echo $timesheet->getTotalTimesheet($user_id, $month);
  exit;
}

function addTimesheet(){
  $request = \Slim\Slim::getInstance()->request();
  $timesheet_info = json_decode($request->getBody());
  require './modal/timesheet.php';
  $timesheet = new Timesheet;
  header('Content-Type: application/json');
  echo $timesheet->addTimesheet($timesheet_info);
  exit;
}

function editTimesheet(){
  $request = \Slim\Slim::getInstance()->request();
  $edit_info = json_decode($request->getBody());
  require './modal/timesheet.php';
  $timesheet = new Timesheet;
  header('Content-Type: application/json');
  echo $timesheet->editTimesheet($edit_info);
  exit;
}

function deleteTimesheet(){
  $request = \Slim\Slim::getInstance()->request();
  $delete_info = json_decode($request->getBody());
  require './modal/timesheet.php';
  $timesheet = new Timesheet;
  header('Content-Type: application/json');
  echo $timesheet->deleteTimesheet($delete_info);
  exit;
}


/*
TODO:
Simple authentication
 */

function authenticate(\Slim\Route $route){
  $app =\Slim\Slim::getInstance();
  $uid= $app->getEncryptedCookie('uid');
  $key = $app->getEncryptedCookie('key');
  if (validateUserKey($uid, $key)==false){
    $app->halt(401);
  }
}
// TODO:
function validateUserKey($uid, $key){

}

?>
