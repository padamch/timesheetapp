<?php
require 'database.php';
class Companies extends Database{
	public function __construct(){}
	
	public function getCompanies($user_id){
		$db =$this->getConnection();
		$sql = "select * FROM timesheet_companies WHERE user_id=:user_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("user_id",$user_id);
		    $stmt->execute();  
		    $companies = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($companies) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":'.$e->getMessage().',"status":0}';
	  	}
	}

	public function getCompany($id){
		$sql = "SELECT * FROM timesheet_companies WHERE id=:id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("id", $id);
		    $stmt->execute();
		    $company = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($company) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":'.$e->getMessage().',"status":0}';
		  }
	}

	public function editCompany($edit_info){			
		$sql = "UPDATE timesheet_companies SET name=:name WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $edit_info->id); 
			$stmt->bindParam("name", $edit_info->name);
			$stmt->execute();
			$db = null;
			$this->addEditedCompany($edit_info->id, $edit_info->user_id);
			return '{"msg":'.json_encode($edit_info).',"status":1}';
		} catch(PDOException $e) {
			return '{"msg":'.$e->getMessage().',"status":0}';
		}	
		
	}


	/*
	* It inserts data into timesheet_edited_timesheets table 
	*/
	public function addEditedCompany($company_id, $user_id){			
		$id='NULL';
		$edited_date=date('Y-m-d');
		$sql = "INSERT INTO timesheet_edited_companies VALUES
		(:id,:company_id,:user_id,:edited_date)";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $id); 
			$stmt->bindParam("company_id", $company_id);
			$stmt->bindParam("user_id", $user_id);
			$stmt->bindParam("edited_date", $edited_date);
			$stmt->execute();
			$db = null;
		} catch(PDOException $e) {
			echo $e->getMessage();
		}	
		
	}


	public function addCompany($company_info){	
		
		if ($this->checkCompany($company_info->user_id, $company_info->name)=='0'){		
			$id='NULL';
			$created_date=date('Y-m-d');

			$sql = "INSERT INTO timesheet_companies VALUES
			(:id,:name,:user_id,:created_date)";
			try {
			    $db = $this->getConnection();
			    $stmt = $db->prepare($sql); 
			    $stmt->bindParam("id", $id); 
			    $stmt->bindParam("name", $company_info->name);
			    $stmt->bindParam("user_id", $company_info->user_id);
			    $stmt->bindParam("created_date", $created_date);
			    $stmt->execute();
			    $company_info->id = $db->lastInsertId();
			    $db = null;
			   	return '{"msg":'.json_encode($company_info).',"status":1}';
			} catch(PDOException $e) {
			    return $e->getMessage();
			}
		} else{ return '{"msg": "Company name already exists!","status":0}';}
		
	}

	public function checkCompany($user_id, $name){
		$sql = "SELECT name FROM timesheet_companies WHERE name=:name and user_id=:user_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("user_id", $user_id);  
		    $stmt->bindParam("name", $name);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }

	}

	/*
	* It deletes company
	*/
	public function deleteCompany($id){			
		$sql = "DELETE FROM timesheet_companies WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $id); 
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted!","status":1}';
		} catch(PDOException $e) {
			return $e->getMessage();
		}	
		
	}

}

?>