<?php
require 'database.php';
class Monthsettings extends Database{
	public function __construct(){}
	
	public function getMonthSetting($user_id,$company_id){
		$db =$this->getConnection();
		$sql = "select * FROM timesheet_month_settings WHERE user_id=:user_id AND company_id=:company_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("user_id",$user_id);
		    $stmt->bindParam("company_id",$company_id);
		    $stmt->execute();  
		    $setting = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($setting) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":'.$e->getMessage().',"status":0}';
	  	}
	}

	public function editMonthSetting($edit_info){			
		$sql = "UPDATE timesheet_month_settings SET start_day=:start_day, end_day=:end_day WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $edit_info->id); 
			$stmt->bindParam("start_day", $edit_info->start_day);
			$stmt->bindParam("end_day", $edit_info->end_day);
			$stmt->execute();
			$db = null;
			return '{"msg":'.json_encode($edit_info).',"status":1}';
		} catch(PDOException $e) {
			return '{"msg":'.$e->getMessage().',"status":0}';
		}	
		
	}


	public function addMonthSetting($setting_info){	
		
		if ($this->checkMonthSetting($setting_info->user_id, $setting_info->company_id)=='0'){		
			$id='NULL';
			$created_date=date('Y-m-d');

			$sql = "INSERT INTO timesheet_month_settings VALUES
			(:id,:start_day,:end_day,:user_id,:company_id,:created_date)";
			try {
			    $db = $this->getConnection();
			    $stmt = $db->prepare($sql); 
			    $stmt->bindParam("id", $id); 
			    $stmt->bindParam("start_day", $setting_info->start_day);
			    $stmt->bindParam("end_day", $setting_info->end_day);
			    $stmt->bindParam("user_id", $setting_info->user_id);
			    $stmt->bindParam("company_id", $setting_info->company_id);
			    $stmt->bindParam("created_date", $created_date);
			    $stmt->execute();
			    $setting_info->id = $db->lastInsertId();
			    $db = null;
			   	return '{"msg":'.json_encode($setting_info).',"status":1}';
			} catch(PDOException $e) {
			    return $e->getMessage();
			}
		} else{ return '{"msg": "Month setting already exists!","status":0}';}
		
	}

	public function checkMonthSetting($user_id, $company_id){
		$sql = "SELECT start_day FROM timesheet_month_settings WHERE user_id=:user_id AND company_id=:company_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("user_id", $user_id);  
		    $stmt->bindParam("company_id", $company_id);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } 
		catch(PDOException $e) {
		    return $e->getMessage();
		}
	}

	/*
	* It deletes company
	*/
	public function deleteMonthSetting($id){			
		$sql = "DELETE FROM timesheet_month_settings WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $id); 
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted!","status":1}';
		} catch(PDOException $e) {
			return $e->getMessage();
		}	
		
	}

}

?>