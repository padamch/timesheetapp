<?php
require 'database.php';
class Timesheet extends Database{
	public function __construct(){}

	public function getTimesheet($user_id,$company_id,$month,$start_item,$items_per_page){
		$sql ='SELECT DATE_FORMAT(date,"%b %d") as date, TIME_FORMAT(time_in, "%H:%i") as time_in,'.
	    'TIME_FORMAT(time_out, "%H:%i") as time_out, '.
	    'TIME_FORMAT(total, "%H:%i") as total '.
	    'FROM timesheet_timesheets '.
	    'WHERE user_id=:user_id '.
	    'AND company_id=:company_id '.
	    'AND MONTH(date)=:month ORDER BY date DESC '. 
	    'LIMIT '.$start_item.','.$items_per_page;

		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("user_id", $user_id);
		    $stmt->bindParam("company_id", $company_id);
		    $stmt->bindParam("month", $month);
		    $stmt->execute();
		    $timesheet = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($timesheet) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		    exit; 
		} catch(PDOException $e) {
		    return '{"msg":'.$e->getMessage().',"status":0}';
		}
	}


	public function getTotalTimesheet($user_id, $month){
	  $sql ='SELECT COUNT(user_id) as "total_items" FROM timesheet_timesheets where user_id=:user_id AND MONTH(date)=:month';
	  try {
	    $db = $this->getConnection();
	    $stmt = $db->prepare($sql);  
	    $stmt->bindParam("user_id", $user_id);
	    $stmt->bindParam("month", $month);
	    $stmt->execute();
	    $total = $stmt->fetchObject();  
	    $db = null;
	    return json_encode($total); 
	  } catch(PDOException $e) {
	    return '{"msg":'.$e->getMessage().',"status":0}';
	  }
	}

	public function addTimesheet($timesheet_info){	
			
		$id='NULL';
		$created_date=date('Y-m-d');
		$sql = "INSERT INTO timesheet_timesheets VALUES
		(:id,:date,:time_in,:time_out,:total,:user_id,:company_id,:created_date)";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $id); 
			$stmt->bindParam("date", $timesheet_info->date);
			$stmt->bindParam("time_in", $timesheet_info->time_in);
			$stmt->bindParam("time_out", $timesheet_info->time_out);
			$stmt->bindParam("total", $timesheet_info->total);
			$stmt->bindParam("user_id", $timesheet_info->user_id);
			$stmt->bindParam("company_id", $timesheet_info->company_id);
			$stmt->bindParam("created_date", $created_date);
			$stmt->execute();
			$timesheet_info->id = $db->lastInsertId();
			$db = null;
			return '{"msg":'.json_encode($timesheet_info).',"status":1}';
		} catch(PDOException $e) {
			return '{"msg":'.$e->getMessage().',"status":0}';
		}	
		
	}

	public function editTimesheet($edit_info){	
		$sql = "UPDATE timesheet_timesheets  SET time_in=:time_in, time_out=:time_out, total=:total ".
				"WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql); 
			$stmt->bindParam("id", $edit_info->id); 
			$stmt->bindParam("time_in", $edit_info->time_in);
			$stmt->bindParam("time_out", $edit_info->time_out);
			$stmt->bindParam("total", $edit_info->total);
			$stmt->execute();
			$db = null;
			$this->addEditedTimesheet($edit_info->id, $edit_info->user_id);
			return '{"msg":'.json_encode($edit_info).',"status":1}';
		} catch(PDOException $e) {
			return '{"msg":'.$e->getMessage().',"status":0}';
		}	
		
	}

	/*
	* It inserts data into timesheet_edited_timesheets table 
	*/
	public function addEditedTimesheet($timesheet_id, $user_id){			
		$id='NULL';
		$edited_date=date('Y-m-d');
		$sql = "INSERT INTO timesheet_edited_timesheets VALUES
		(:id,:timesheet_id,:user_id,:edited_date)";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $id); 
			$stmt->bindParam("timesheet_id", $timesheet_id);
			$stmt->bindParam("user_id", $user_id);
			$stmt->bindParam("edited_date", $edited_date);
			$stmt->execute();
			$db = null;
		} catch(PDOException $e) {
			echo $e->getMessage();
		}	
		
	}

	/*
	* It inserts data into timesheet_edited_timesheets table 
	*/
	public function deleteTimesheet($delete_info){			
		$sql = "DELETE FROM timesheet_timesheets WHERE id=:id";
		try {
			$this->addDeletedTimesheet($delete_info->timesheet_id); // Record to deleted timesheet
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $delete_info->timesheet_id); 
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted!","status":1}';
		} catch(PDOException $e) {
			return '{"msg":'.$e->getMessage().',"status":0}';
		}	
		
	}

	/*
	* It inserts data into timesheet_deleted_timesheets table 
	* TODO: it doesn't add data but shows no error!
	*/
	private function addDeletedTimesheet($timesheet_id){			
		$sql = "INSERT INTO timesheet_deleted_timesheets ".
				"(date, time_in, time_out, total, user_id, created_date,deleted_date) ".
    			"SELECT date, time_in, time_out, total, user_id, created_date, current_date() ".
    			"FROM  timesheet_timesheets where id = :id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $timesheet_id);
			$stmt->execute();
			$db = null;
			return 'added..';
		} catch(PDOException $e) {
			return $e->getMessage();
		}	
		
	}


}
?>