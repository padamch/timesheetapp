<?php
require 'database.php';
class Users extends Database{
	public function __construct(){}
	
	public function getUsers(){
		$db =$this->getConnection();
		$sql = "select * FROM timesheet_users ORDER BY username";
		try {
		    $stmt = $db->query($sql);  
		    $users = $stmt->fetchAll(PDO::FETCH_OBJ);
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($users) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":'.$e->getMessage().',"status":0}';
	  	}
	}

	public function getUser($id){
		$sql = "SELECT * FROM timesheet_users WHERE id=:id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("id", $id);
		    $stmt->execute();
		    $user = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($user) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":'.$e->getMessage().',"status":0}';
		  }
	}

	/**
	*
	*
	*/
	public function login($user){
		$password = $this->create('sha256', $user->password);
		$sql = "SELECT username, password FROM timesheet_users WHERE username=:username AND password=:password";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("username", $user->username);
		    $stmt->bindParam("password", $password);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    if($rowCount=='1') return $this->userData($user->username);
		    else return '{"msg": "username or password wrong!","status":0}';
		}catch(PDOException $e) {
		    return '{"msg":'.$e->getMessage().',"status":0}';
		}

	}

	//it returns userdata: {id, username,email,role,created_date}
	public function userData($username){
		$sql = "SELECT id, username, email, role, created_date FROM timesheet_users WHERE username=:username";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("username", $username);
		    $stmt->execute();
		    $user = $stmt->fetchObject(); 
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($user) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		}catch(PDOException $e) {
		    return '{"msg":'.$e->getMessage().',"status":0}';
		}

	}

	public function addUsers($users){	
		
		if ($this->checkUsername($users->username)=='0' && 
		$this->checkEmail($users->email)=='0'){
			
			$id='NULL';
			$password = $this->create('sha256', $users->password);
			$created_date=date('Y-m-d');
			$role='default';

			$sql = "INSERT INTO timesheet_users VALUES
			(:id,:username,:email,:password,:role,:created_date)";
			try {
			    $db = $this->getConnection();
			    $stmt = $db->prepare($sql); 
			    $stmt->bindParam("id", $id); 
			    $stmt->bindParam("username", $users->username);
			    $stmt->bindParam("email", $users->email);
			    $stmt->bindParam("password", $password);
			    $stmt->bindParam("role", $role);
			    $stmt->bindParam("created_date", $created_date);
			    $stmt->execute();
			    $this->sendMailUser($users->email,$users->username);
			    $users->id = $db->lastInsertId();
			    $db = null;
			    return '{"msg": ' . json_encode($users) . ',"status":1}';
			   	//return json_encode($users);
			} catch(PDOException $e) {
			    return '{"msg":'.$e->getMessage().',"status":0}';
			}
		} else{ return '{"msg":"username or email already exists","status":0}';}
		
	}

	public function checkUsername($username){
		$sql = "SELECT username FROM timesheet_users WHERE username=:username";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("username", $username);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }

	}

	public function checkEmail($email){
		$sql = "SELECT email FROM timesheet_users WHERE email=:email";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("email", $email);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }
	}

	/**
	* It sends User created confirmation mail
	*/
	private function sendMailUser($email, $username){
		$subject='Welcome to timesheet';
		$message='Thank you for choosing us.'."\r\n Your username: ". $username;
		$headers = 'From: no-reply@timesheet.com' . "\r\n" .
	    'Reply-To: webmaster@timesheet.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();
		mail($email,$subject,$message,$headers);
	}

	public function forgetPassword($user_info){
		
		if ($this->checkEmail($user_info->email)=='1'){
			
			$id='NULL';
			$code =rand(100, 999);//3 digits random number
			$created_date=date('Y-m-d');
			$sql ="INSERT INTO timesheet_forget_passwords VALUES(:id,:code,:email,:created_date)";
			try {
			    $db = $this->getConnection();
			    $stmt = $db->prepare($sql); 
			    $stmt->bindParam("id", $id); 
			    $stmt->bindParam("code", $code);
			    $stmt->bindParam("email", $user_info->email);
			    $stmt->bindParam("created_date", $created_date);
			    $stmt->execute();
			    $this->sendMailForgetPassword($user_info->email, $code);
			    $db = null;
			   	return ('{"msg":"The code is sent to your email. Please, check your email.","status":1}');
			} catch(PDOException $e) {
			    return '{"msg":'.$e->getMessage().',"status":0}';
			}
		} else{ return '{"msg":"Email address does not exist","status":0}';}
	}

	/**
	* It sends email with 'code' to reset the forgotten password 
	*/
	private function sendMailForgetPassword($email, $code){
		$subject='Reset password!';
		$message='Your Code: '. $code;
		$headers = 'From: no-reply@timesheet.com' . "\r\n" .
	    'Reply-To: webmaster@timesheet.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();
		mail($email,$subject,$message,$headers);
	}

	public function resetPassword($user_info){
		
		if ($this->checkCode($user_info->email,$user_info->code)=='1'){		
			$password = $this->create('sha256', $user_info->password);
			$email =$user_info->email;
			$sql ="UPDATE timesheet_users SET password=:password WHERE email=:email";
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam("password", $password);
				$stmt->bindParam("email", $email);
				$stmt->execute();
				//$stmt->execute(array(':password'=>$password,':email'=>$email));
				$this->sendMailResetPassword($user_info->email);
				$db = null;
				$this->deleteCode($email, $user_info->code);//Delete the code from DB when the pwd is reset.
				return '{"msg":"Your password is successfully reset.","status":1}';
			} catch(PDOException $e) {
				return '{"msg":'.$e->getMessage().',"status":0}';
			}
		} else{return '{"msg":"oops, wrong code!","status":0}';}
		
	}

	/**
	* It sends email with 'code' to reset the forgotten password 
	*/
	private function sendMailresetPassword($email){
		$subject='Password changed!';
		$message='Your password has been changed';
		$headers = 'From: no-reply@timesheet.com' . "\r\n" .
	    'Reply-To: webmaster@timesheet.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();
		mail($email,$subject,$message,$headers);
	}


	/*
	* it checkes the code corresponding to emails.
	*/	
	public function checkCode($email, $code){
		$sql = "SELECT * FROM timesheet_forget_passwords WHERE email=:email AND code=:code";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("email", $email);
		    $stmt->bindParam("code", $code);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }
	}

	/*
	* it deletes the code corresponding to emails.
	*/	
	private function deleteCode($email, $code){
		$sql = "DELETE FROM timesheet_forget_passwords WHERE email=:email AND code=:code";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("email", $email);
		    $stmt->bindParam("code", $code);
		    $stmt->execute();
		    $db = null;
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }
	}


	/**
     *
     * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
     * @param string $data The data to encode
     * @param string $salt The salt (This should be the same throughout the system probably)
     * @return string The hashed/salted data
     */
    public function create($algo, $data)
    {      
        $salt='100yearsoldman';
        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);       
        return hash_final($context);       
    }

}

?>