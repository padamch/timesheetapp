<?php
class Database{

    public function __construct(){}

    public function getConnection(){
        require_once './config.php';
        $dbh = new PDO(DB_TYPE.":dbname=".DB_NAME.";host=".DB_HOST, DB_USER, DB_PASS);  
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbh;
    }
}
?>