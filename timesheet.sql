/* timesheet beta*/

DROP database IF EXISTS timesheet;
CREATE database timesheet;

USE timesheet;
DROP TABLE IF EXISTS timesheet_users;
DROP TABLE IF EXISTS timesheet_companies;
DROP TABLE IF EXISTS timesheet_edited_companies;
DROP TABLE IF EXISTS timesheet_month_settings;
DROP TABLE IF EXISTS timesheet_timesheets;
DROP TABLE IF EXISTS timesheet_forget_passwords;
DROP TABLE IF EXISTS timesheet_edited_timesheets;
DROP TABLE IF EXISTS timesheet_deleted_timesheets;

CREATE TABLE timesheet_users(
  id INT NOT NULL AUTO_INCREMENT,
  username varchar(50) UNIQUE,
  email varchar(60) UNIQUE,
  password varchar(64),
  role enum('default','admin','owner') NOT NULL DEFAULT 'default',
  created_date DATE,  
  PRIMARY KEY(id)  
)ENGINE=INNODB;

INSERT INTO timesheet_users VALUES 
  (NULL,'kasper','kasper@yahoo.com','3327a2154aa1900fa110ae3d20d27d051ba719ead0396f1a23d6865b2677ed4a','default','2014-03-10'),
  (NULL,'allan','allan@yahoo.com','3327a2154aa1900fa110ae3d20d27d051ba719ead0396f1a23d6865b2677ed4a','default','2014-02-10'),
  (NULL,'john','john@hotmail.com','3327a2154aa1900fa110ae3d20d27d051ba719ead0396f1a23d6865b2677ed4a','default','2014-01-10'),
  (NULL,'padam','padam101@hotmail.com','00b98efcb45aae5327bbcc79f93ce44ed81f7d2eabeed112792b6ab5c0fa2393','admin','2014-02-10');
  
CREATE TABLE timesheet_forget_passwords(
  id INT NOT NULL AUTO_INCREMENT,
  code INT,
  email varchar(60) UNIQUE,
  created_date DATE,
  PRIMARY KEY(id)
)ENGINE=INNODB;

CREATE TABLE timesheet_companies(
  id INT NOT NULL AUTO_INCREMENT,
  name varchar(100),
  user_id INT,
  created_date DATE,
  PRIMARY KEY(id),
  FOREIGN KEy(user_id) REFERENCES timesheet_users(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

INSERT INTO timesheet_companies VALUES
  (NULL, 'Cafe Zirup',1,'2014-3-15'),
  (NULL, 'Cafe Halifax',1,'2014-3-15'),
  (NULL, 'Wagamama',2,'2014-3-15'),
  (NULL, 'Hoppes',3,'2014-3-15'),
  (NULL, 'Zirup Cafe',4,'2014-3-15');

CREATE TABLE timesheet_edited_companies(
  id INT NOT NULL AUTO_INCREMENT,
  company_id INT,
  user_id INT,
  edited_date DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(company_id) REFERENCES timesheet_companies(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(user_id) REFERENCES timesheet_users(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

CREATE TABLE timesheet_month_settings(
  id INT NOT NULL AUTO_INCREMENT,
  start_day INT,
  end_day INT,
  user_id INT,
  company_id INT,
  created_date DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(user_id) REFERENCES timesheet_users(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(company_id) REFERENCES timesheet_companies(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

INSERT INTO timesheet_month_settings VALUES
  (NULL,15,14,4,5,'2014-3-15');

CREATE TABLE timesheet_timesheets(
  id INT NOT NULL AUTO_INCREMENT,
  date DATE,
  time_in TIME,
  time_out TIME,
  total TIME,
  user_id INT,
  company_id INT,
  created_date DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(user_id) REFERENCES timesheet_users(id) ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=INNODB;

INSERT INTO timesheet_timesheets VALUES 
  (NULL,'2014-02-14','09:00','15:30','06:30',1,1,'2014-03-10'),
  (NULL,'2014-02-15','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-16','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-17','09:00','15:50','06:50',1,1,'2014-03-10'),
  (NULL,'2014-02-18','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-19','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-20','09:00','15:00','06:00',1,1,'2014-02-10'),
  (NULL,'2014-02-21','09:00','15:00','06:00',1,1,'2014-01-10'),
  (NULL,'2014-02-22','09:00','15:00','06:00',1,1,'2014-01-10'),
  (NULL,'2014-02-23','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-24','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-25','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-26','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-27','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-02-28','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-01','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-02','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-03','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-04','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-05','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-06','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-07','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-08','09:00','15:00','06:00',1,1,'2014-03-10'),
  (NULL,'2014-03-09','09:00','15:00','06:00',1,2,'2014-03-10'),
  (NULL,'2014-03-10','09:00','15:00','06:00',1,2,'2014-03-10'),
  (NULL,'2014-03-11','09:00','15:00','06:00',1,2,'2014-03-10'),
  (NULL,'2014-03-12','09:00','15:00','06:00',1,2,'2014-03-10'),
  (NULL,'2014-03-13','09:00','15:00','06:00',1,2,'2014-03-10'),
  (NULL,'2014-03-14','09:00','15:00','06:00',1,2,'2014-03-10'),
  (NULL,'2014-02-11','15:00','23:00','06:00',2,3,'2014-03-10'),
  (NULL,'2014-01-11','09:00','16:00','07:00',3,4,'2014-03-10');

CREATE TABLE timesheet_edited_timesheets(
  id INT NOT NULL AUTO_INCREMENT,
  timesheet_id INT,
  user_id INT,
  edited_date DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(user_id) REFERENCES timesheet_users(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(timesheet_id) REFERENCES timesheet_timesheets(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

CREATE TABLE timesheet_deleted_timesheets(
  id INT NOT NULL AUTO_INCREMENT,
  date DATE,
  time_in TIME,
  time_out TIME,
  total TIME,
  user_id INT,
  created_date DATE,
  deleted_date DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(user_id) REFERENCES timesheet_users(id) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;
